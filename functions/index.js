/* eslint-disable promise/catch-or-return */
/* eslint-disable promise/no-nesting */
const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp()
const cors = require('cors')
const express = require('express')
const app = express()
const boolParser = require('express-query-boolean')
app.use(cors({origin: true}))
app.use(boolParser());

const db = admin.firestore()
const settings = {/* your settings... */ timestampsInSnapshots: true};
db.settings(settings);

// const faker = require('faker')

function calculateScore(query, value) {
  let score = 0
  if (query.experienced === value.experienced) score += 0.3
  if (query.latitude === value.geolocation._latitude) score += 0.1
  if (query.longitude === value.geolocation._longitude) score += 0.1
  // if (query.latitude <= value.geolocation._latitude || query.longitude >= value.geolocation._longitude) score += 0.1
  if (Number(query.monthly_income) === value.monthly_income) score += 0.2
  if (Number(query.monthly_income) <= value.monthly_income) score += 0.2
  if (Number(query.monthly_income) >= value.monthly_income) score += 0.1
  if (Number(query.age) === value.age) score += 0.2
  // if (Number(query.age) <= value.age || Number(query.age) >= value.age) score += 0.2
  return score
}

app.get('/people-like-you', async (req, res) => {
  var result = []
  // return res.send(req.query)
  var people = db.collection('people').orderBy('score', 'desc').limit(10)
  people.orderBy('score', 'desc')
  var transactionUpdate = await db.runTransaction(async t => {
    try {
      const doc = await t.get(people)
      doc.forEach(v => {
        try {
          var id = db.collection('people').doc(v.id)
          t.update(id, { score: 0 })
          let score = calculateScore(req.query, v.data())
          t.update(id, { score: score })
        }
        catch (error) {
          console.log(error)
        }
      })

      return console.log('updated')
    }
    catch (err) {
      return res.send(err)
    }
  })

  var transactionGet = await db.runTransaction(async t => {
    try {
      const doc = await t.get(people)
      result = [];
      doc.forEach(v => {
        let data = v.data()
        data.name = v.id
        if (data.score !== 0) result.push(data)
      })
      return res.send(result);
    }
    catch (err) {
      return console.log(err);
    }
  })
})

// app.get('/create-data', (req, res) => {
//   var people = db.collection('people')
//   for (let index = 0; index < 25; index++) {
//     people.doc(faker.name.findName()).set({
//       age: faker.random.number({
//         min: 19,
//         max: 45
//       }),
//       experienced: faker.random.boolean(),
//       geolocation: {
//         _latitude: faker.address.latitude(),
//         _longitude: faker.address.longitude()
//       },
//       monthly_income: faker.random.number({
//         min: 2100,
//         max: 8700
//       })
//     })
//   }

//   res.send('Data created');

// })

const searchPeople = express().use('/api', app)
exports.searchPeople = functions.https.onRequest(searchPeople)
